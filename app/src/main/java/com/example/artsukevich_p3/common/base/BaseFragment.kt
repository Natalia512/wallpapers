package com.example.artsukevich_p3.common.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigator
import androidx.navigation.fragment.findNavController
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.util.view.findNavController
import com.example.artsukevich_p3.util.view.dialogBuilder

import com.example.artsukevich_p3.util.view.toast
import com.google.android.material.bottomnavigation.BottomNavigationView

open class BaseFragment(@LayoutRes layoutId:Int): Fragment(layoutId) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun toast(messageStringRes:String){
        requireContext().toast(messageStringRes)
    }

    fun navigate(resId:Int){
        this.findNavController().navigate(resId)
    }
    fun navigate(direction: NavDirections,extras: Navigator.Extras){
        this.findNavController().navigate(direction,extras)
    }
    fun navigate(direction: NavDirections){
        this.findNavController().navigate(direction)
    }
    fun navigateBack(){
        this.findNavController().popBackStack()
    }

}