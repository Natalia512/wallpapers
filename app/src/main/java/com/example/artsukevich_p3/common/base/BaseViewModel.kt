package com.example.lesson22.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel<T>():ViewModel() {
    protected val queries: MutableLiveData<List<T>> by lazy {
        MutableLiveData<List<T>>()
    }
    fun getQueries(): LiveData<List<T>> {
        return queries
    }
}