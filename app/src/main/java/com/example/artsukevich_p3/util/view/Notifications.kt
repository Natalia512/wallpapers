package com.example.artsukevich_p3.util.view

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.artsukevich_p3.R

class Notifications {
    val CHANNEL_ID = "channel"
    fun createNotificationChanel(context: Context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, "My channel", NotificationManager.IMPORTANCE_HIGH)
            channel.description = "My channel description"
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.vibrationPattern = longArrayOf(200,200,200,200)
            channel.enableVibration(true)
            Log.i("C",channel.shouldVibrate().toString())
            Log.i("Cc",channel.shouldShowLights().toString())
            val notificationManager: NotificationManager =
                context.getSystemService(NotificationManager::class.java) as NotificationManager
            notificationManager.createNotificationChannel(channel);
        }
    }

    fun buildNotification(
        context: Context,
        title: String,
        text: String
    ): NotificationCompat.Builder {
        var builder =
            NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notifications)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
        return builder
    }
}