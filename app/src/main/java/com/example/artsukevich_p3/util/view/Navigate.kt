package com.example.artsukevich_p3.util.view

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.navigation.NavController


fun Context.findNavController(): NavController {
  return this.findNavController()
}
