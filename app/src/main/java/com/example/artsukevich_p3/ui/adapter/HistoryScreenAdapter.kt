package com.example.artsukevich_p3.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.databinding.HistoryItemBinding
import com.example.domain.model.EntityQuery
import java.text.SimpleDateFormat
import java.util.*

class HistoryScreenAdapter(private val clickImage:(item: EntityQuery)->Unit) :
    ListAdapter<EntityQuery, HistoryScreenAdapter.PhotoViewHolder>(PhotoDiffUtil()) {

    open inner class PhotoViewHolder(private val binding: HistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EntityQuery) = with(itemView) {

            val userDob = SimpleDateFormat("dd/M/yyyy hh:mm:ss").parse(item.date)
            val today = Date()
            var different = today.time - userDob.time
            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60
            val daysInMilli = hoursInMilli * 24
            val elapsedDays: Long = different / daysInMilli
            different = different % daysInMilli

            val elapsedHours: Long = different / hoursInMilli
            different = different % hoursInMilli

            val elapsedMinutes: Long = different / minutesInMilli
            different = different % minutesInMilli

            val elapsedSeconds: Long = different / secondsInMilli
            //  minutes.toString()+" minutes "
            binding.title.text=item.query
            binding.results.text=item.results
            if(elapsedMinutes>=59 && elapsedHours<=24  ){
                binding.date.text= elapsedHours.toString()  +" hours "
            }else{
                binding.date.text= elapsedMinutes.toString()  +" minutes "

            }

            if(elapsedHours>=24 && elapsedDays<3 ){
                binding.date.text= elapsedDays.toString()  +" days "
            }
            if(elapsedDays>3)
            {    binding.date.text=SimpleDateFormat("dd/M/yyyy ").format(Date())}




if (!item.fav){
    binding.fav.setImageResource(R.drawable.ic_baseline_favorite_24)
}else{
    binding.fav.setImageResource(R.drawable.ic_click_favorite_)
}
            binding.fav.setOnClickListener {
              //  binding.fav.setBackgroundColor(Color.YELLOW)
                item.fav=true
                binding.fav.setImageResource(R.drawable.ic_click_favorite_)
                clickImage(item)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = HistoryItemBinding.inflate(view, parent, false)
        return PhotoViewHolder(binding)
    }



    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
        Log.i("Q3","item.toString()")
    }

    class PhotoDiffUtil() : DiffUtil.ItemCallback<EntityQuery>() {

        override fun areItemsTheSame(oldItem: EntityQuery, newItem: EntityQuery): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: EntityQuery, newItem: EntityQuery): Boolean {
            return oldItem.query == newItem.query
        }
    }
    }