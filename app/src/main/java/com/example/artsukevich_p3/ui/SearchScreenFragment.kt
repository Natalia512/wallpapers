package com.example.artsukevich_p3.ui

import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.flatMap
import androidx.paging.map
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.artsukevich_p3.InternetReceiver
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.SearchScreenBinding
import com.example.artsukevich_p3.ui.adapter.PagingAdapter
import com.example.artsukevich_p3.ui.adapter.SearchScreenAdapterFourGrid
import com.example.artsukevich_p3.ui.viewmodels.ViewModelFactory
import com.example.artsukevich_p3.ui.viewmodels.ViewModelPhoto
import com.example.artsukevich_p3.ui.viewmodels.ViewModelQuery
import com.example.data.mapper.mapToPhotosList
import com.example.domain.model.NavigateModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class SearchScreenFragment() : BaseFragment(R.layout.search_screen) {

    @Volatile
    var totalItems = 0
    val viewModelQuery = ViewModelQuery()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = SearchScreenBinding.bind(view)

        val viewModelFactory = ViewModelFactory()
        val viewModelPhotos =
            ViewModelProvider(this, viewModelFactory).get(ViewModelPhoto::class.java)
        val broadcastReceiver: BroadcastReceiver = InternetReceiver(binding.connect)
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        val recyclerView = binding.recyclerView
        val adapterPaging =
            PagingAdapter(fun(image: ImageView, photo: NavigateModel) { clickImage(photo, image) })
        val adapterFourGrid =
            SearchScreenAdapterFourGrid(fun(image: ImageView, photo: NavigateModel) {
                clickImage(
                    photo,
                    image
                )
            })
        val gridFourImage = binding.grid
        var str: String? =null
        var isSetGrid = true


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.requireContext().registerReceiver(broadcastReceiver, intentFilter)
        }

        recyclerView.adapter = adapterPaging
        recyclerView.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        binding.inputText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                str = query
                if (!isSetGrid) {
                    fourGrid(recyclerView, adapterFourGrid, viewModelPhotos, str)

                } else {
                    sixGrid(recyclerView, adapterPaging, viewModelPhotos, str)
                }
                Log.i("total", totalItems.toString())
                viewModelQuery.createEntityQueryModel(query, totalItems.toString())
                return true
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                str = newText
                if (!isSetGrid) {
                    fourGrid(recyclerView, adapterFourGrid, viewModelPhotos, str)
                } else {
                    sixGrid(recyclerView, adapterPaging, viewModelPhotos, str)
                }
                return true
            }
        }).apply {
            Handler().postDelayed( {viewModelQuery.createEntityQueryModel(str, totalItems.toString())},4000)
        }


        gridFourImage.setOnClickListener {
            if (isSetGrid) {
                isSetGrid = false
                gridFourImage.setImageResource(R.mipmap.ic_four_grid2_2_foreground)
                fourGrid(recyclerView, adapterFourGrid, viewModelPhotos, str)
            } else {
                isSetGrid = true
                gridFourImage.setImageResource(R.drawable.ic_six_grid)
                sixGrid(recyclerView, adapterPaging, viewModelPhotos, str)
            }
        }
    }

    fun sixGrid(
        recyclerView: RecyclerView,
        adapterPaging: PagingAdapter,
        viewModelPhotos: ViewModelPhoto,
        query: String?
    ) {
        recyclerView.adapter = adapterPaging
        recyclerView.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        lifecycleScope.launch {
            viewModelPhotos.search(query).collectLatest { pagedData ->
                val res = pagedData.map {
                    totalItems = it.total
                    it
                }.flatMap { it.result.mapToPhotosList() }
                adapterPaging.submitData(res)
            }
        }

    }

    fun fourGrid(
        recyclerView: RecyclerView,
        adapterFourGrid: SearchScreenAdapterFourGrid,
        viewModelPhotos: ViewModelPhoto,
        query: String?
    ) {
        recyclerView.adapter = adapterFourGrid
        recyclerView.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        lifecycleScope.launch {
            viewModelPhotos.search(query).collectLatest { pagedData ->
                val res = pagedData.map {
                    totalItems = it.total
                    it
                }.flatMap { it.result.mapToPhotosList() }
                adapterFourGrid.submitData(res)
            }
        }
    }

    fun clickImage(photo: NavigateModel, imageView: ImageView) {
        val action =
            SearchScreenFragmentDirections.actionSearchScreenFragmentToImageDetailsScreenFragment(
                photo
            )
        navigate(action)
    }

}
