package com.example.artsukevich_p3.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.ImageFullscreenBinding

class ImageFullscreenFragment : BaseFragment(R.layout.image_fullscreen) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ImageFullscreenBinding.bind(view)
        val args: ImageFullscreenFragmentArgs by navArgs()
        val arg = args.navModel
        Glide.with(this)
            .load(arg.url)
            .into(binding.imageFullScreen)
        binding.fab.setOnClickListener {
            navigateBack()
        }
    }
}