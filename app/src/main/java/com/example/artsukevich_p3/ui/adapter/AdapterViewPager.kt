package com.example.artsukevich_p3.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.artsukevich_p3.databinding.RecyclerBinding

class AdapterViewPager(val adapter: FavouritesPhotoAdapter,val adapter2: FavouritesQueriesAdapter) : RecyclerView.Adapter<AdapterViewPager.PhotoViewHolder>() {
    open inner class PhotoViewHolder(private val binding: RecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val recycler=binding.recyclerView

        fun bind(position: Int) = with(itemView) {
            if (position==0){
                recycler.adapter=adapter
                recycler.layoutManager=LinearLayoutManager(context)
            }else{
                recycler.adapter=adapter2
                recycler.layoutManager=LinearLayoutManager(context)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = RecyclerBinding.inflate(view, parent, false)
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(position)
        Log.i("Q3","item.toString()")
    }

    override fun getItemCount()=2

}