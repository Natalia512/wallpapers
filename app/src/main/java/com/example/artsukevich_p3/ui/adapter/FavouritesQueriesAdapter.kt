package com.example.artsukevich_p3.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.databinding.FavouriteItemQueryBinding
import com.example.domain.model.EntityFavouriteQuery

class FavouritesQueriesAdapter (private val clearQuery:(item: EntityFavouriteQuery)->Unit) :
    ListAdapter<EntityFavouriteQuery, FavouritesQueriesAdapter.PhotoViewHolder>(PhotoDiffUtil()) {

    open inner class PhotoViewHolder(private val binding: FavouriteItemQueryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EntityFavouriteQuery) = with(itemView) {
            val animationController = AnimationUtils.loadAnimation(context, R.anim.fall_down)
            itemView.animation=animationController
                binding.title.text=item.query
                binding.results.text=item.results
                binding.date.text=item.date

            binding.clear.setOnClickListener {
                clearQuery(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = FavouriteItemQueryBinding.inflate(view, parent, false)
        return PhotoViewHolder(binding)
    }



    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
        Log.i("Q3","item.toString()")
    }

    class PhotoDiffUtil() : DiffUtil.ItemCallback<EntityFavouriteQuery>() {

        override fun areItemsTheSame(oldItem: EntityFavouriteQuery, newItem: EntityFavouriteQuery): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: EntityFavouriteQuery, newItem: EntityFavouriteQuery): Boolean {
            return oldItem.query == newItem.query
        }
    }


}