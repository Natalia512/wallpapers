package com.example.artsukevich_p3.ui.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.data.model.pojo.photo.Photos
import com.example.artsukevich_p3.databinding.SearchScreenFourGridBinding
import com.example.domain.model.NavigateModel

class SearchScreenAdapterFourGrid (private val clickImage:(image: ImageView, photo: NavigateModel)->Unit) : PagingDataAdapter<NavigateModel, SearchScreenAdapterFourGrid.PhotoViewHolder>(PhotoDiffUtil()
){

    open inner class PhotoViewHolder(private val binding: SearchScreenFourGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val image=binding.itemFourGrid
        fun bind(item: NavigateModel) = with(itemView) {

            Glide.with(context)
                .load(item.url)
                .placeholder(ColorDrawable(Color.parseColor(item.color)))
                .into(image)

            image.setOnClickListener {
                clickImage(image,item)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = SearchScreenFourGridBinding.inflate(view, parent, false)
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    class PhotoDiffUtil() : DiffUtil.ItemCallback<NavigateModel>() {

        override fun areItemsTheSame(oldItem: NavigateModel, newItem: NavigateModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NavigateModel, newItem: NavigateModel): Boolean {
            return oldItem == newItem
        }
    }
}