package com.example.artsukevich_p3.ui

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.HistoryScreenBinding
import com.example.artsukevich_p3.ui.adapter.HistoryScreenAdapter
import com.example.artsukevich_p3.ui.viewmodels.ViewModelFavouriteQuery
import com.example.artsukevich_p3.ui.viewmodels.ViewModelQuery
import com.example.domain.model.EntityFavouriteQuery
import com.example.domain.model.EntityQuery


class HistoryScreenFragment : BaseFragment(R.layout.history_screen) {
    val viewModel = ViewModelQuery()
    val viewModelFavouriteQuery = ViewModelFavouriteQuery()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = HistoryScreenBinding.bind(view)
        val recyclerView = binding.recyclerView
        val adapter = HistoryScreenAdapter(fun(item: EntityQuery) { update(item) })

        viewModel.loadQueries()
        viewModel.getQueries().observe(viewLifecycleOwner, Observer {
            val animationController =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation)
            recyclerView.setLayoutAnimation(animationController)
            adapter.submitList(it)

        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this.requireContext())

    }

    fun update(item: EntityQuery) {
        try {
            val fav = EntityFavouriteQuery()
            fav.id = item.id
            fav.date = item.date
            fav.results = item.results
            fav.query = item.query
            toast("QQQ")
            viewModelFavouriteQuery.insertQuery(fav)
            viewModel.updateQuery(item)
        } catch (e: SQLiteConstraintException) {
            toast("This query already exist in favourites")
        }
    }
}