package com.example.artsukevich_p3.ui.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.artsukevich_p3.databinding.SearchScreenItemBinding
import com.example.domain.model.NavigateModel

class PagingAdapter(private val clickImage: (image: ImageView, photo: NavigateModel) -> Unit) :
    PagingDataAdapter<NavigateModel, PagingAdapter.PhotoViewHolder>(PhotoDiffUtil()) {
    lateinit var binding: SearchScreenItemBinding
    var count = 0

    open inner class PhotoViewHolder(private val binding: SearchScreenItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val shimmerContainer = binding.shimmerContainer

        val image = binding.searchPhoto
        fun bind(item: NavigateModel) = with(itemView) {
            count++
            Log.i("eeee", count.toString())
            shimmerContainer.startShimmer()
            Glide.with(context)
                .load(item.url).listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        Log.i("qq", "ff")
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        shimmerContainer.stopShimmer()
                        shimmerContainer.setShimmer(null)
                        return false
                    }

                })
                .placeholder(ColorDrawable(Color.parseColor(item.color)))
                .into(image)

            image.setOnClickListener {
                clickImage(image, item)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        Log.i("l3", "item.toString()")
        binding = SearchScreenItemBinding.inflate(view, parent, false)

        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PagingAdapter.PhotoViewHolder, position: Int) {

        holder.bind(getItem(position)!!)

    }

    class PhotoDiffUtil() : DiffUtil.ItemCallback<NavigateModel>() {

        override fun areItemsTheSame(oldItem: NavigateModel, newItem: NavigateModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NavigateModel, newItem: NavigateModel): Boolean {
            return oldItem == newItem
        }
    }
}