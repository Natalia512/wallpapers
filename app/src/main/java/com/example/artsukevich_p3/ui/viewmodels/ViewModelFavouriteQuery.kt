package com.example.artsukevich_p3.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.data.model.repository.RepositoryFavouriteQueries
import com.example.domain.model.EntityFavouriteQuery
import com.example.lesson22.ui.base.BaseViewModel

class ViewModelFavouriteQuery: BaseViewModel<EntityFavouriteQuery>() {
    val repo: RepositoryFavouriteQueries = RepositoryFavouriteQueries()


    fun loadQueries(){
        val a=repo.getQueries()
        queries.postValue(a)
    }
    fun insertQuery(query: EntityFavouriteQuery){
        repo.setQuery(query)
    }

    fun deleteQuery(query: EntityFavouriteQuery){
        repo.deleteQuery(query)
    }

}