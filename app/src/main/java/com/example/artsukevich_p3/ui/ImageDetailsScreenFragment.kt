package com.example.artsukevich_p3.ui

import android.app.WallpaperManager
import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.ImageDetailsScreenBinding
import com.example.artsukevich_p3.ui.viewmodels.ViewModelPhoto
import com.example.domain.model.NavigateModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.io.*


class ImageDetailsScreenFragment : BaseFragment(R.layout.image_details_screen) {
    val viewModelPhoto: ViewModelPhoto = ViewModelPhoto()
    lateinit var binding: ImageDetailsScreenBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ImageDetailsScreenBinding.bind(view)
        val args: ImageDetailsScreenFragmentArgs by navArgs()
        val navigateModel = args.navModel
        val back = binding.toolbar


        Glide.with(this)
            .load(navigateModel.url)
            .into(binding.imageDetailsScreen)
       // val anim = AnimationUtils.loadAnimation(requireContext(),R.anim.slide_out_info)
       // binding.navigation.startAnimation(anim)
        binding.navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_share -> {
                    shareSheet(args.navModel)
                    true
                }
                R.id.action_settings -> {
                    settingsSheet(navigateModel)
                    true
                }
                R.id.action_info -> {
                    val action =
                        ImageDetailsScreenFragmentDirections.actionImageDetailsScreenFragmentToImageInformationScreen(
                            navigateModel
                        )
                    navigate(action)
                    true
                }
                else -> true
            }
        }


        binding.fab.setOnClickListener {
            val action =
                ImageDetailsScreenFragmentDirections.actionImageDetailsScreenFragmentToImageFullscreenFragment(
                    navigateModel
                )
            navigate(action)
        }


        back.setNavigationIcon(R.drawable.ic_back)
        back.setOnClickListener {
            navigateBack()
        }
    }

    fun getUri(title: String?, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path: String = MediaStore.Images.Media.insertImage(
            this.requireContext().contentResolver,
            bitmap, title + ".png",
            ""
        )
        return Uri.parse(path)
    }

    fun downloadFavourite(title: String?, bitmap: Bitmap) {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path: String = MediaStore.Images.Media.insertImage(
            this.requireContext().contentResolver,
            bitmap, title + ".png",
            ""
        )
    }

    private fun shareSheet(args: NavigateModel) {
        Glide.with(this).asBitmap()
            .load(args.url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    try {
                        val intent = Intent()
                        intent.action = Intent.ACTION_SEND
                        intent.putExtra(Intent.EXTRA_STREAM, getUri(args.userMail, resource))
                        intent.type = "image/*"
                        startActivity(Intent.createChooser(intent, "Share: "))
                    }catch (e:SecurityException){
                        toast("this photo not can be download")
                    }


                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
    }

    var count = 0
    private fun settingsSheet(args: NavigateModel) {
        val bottomSheetDialog = BottomSheetDialog(this.requireContext())
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog_layout)
        bottomSheetDialog.findViewById<TextView>(R.id.favourite)?.setOnClickListener {
            try {
                viewModelPhoto.insertPhotos(args)
                toast("EEE")
            } catch (e: SQLiteConstraintException) {
                toast("This photo already exist in favourites")
            }

        }

        bottomSheetDialog.findViewById<TextView>(R.id.mainScreen)?.setOnClickListener {
            if (count == 0) {
                val wallpaperManager =
                    WallpaperManager.getInstance(this.requireContext().applicationContext)
                Glide.with(it).asBitmap().load(args.url).into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        wallpaperManager.setBitmap(resource)
                        downloadFavourite(args.userMail, resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                })
            } else {
                toast("This photo already exist in main screen")
            }
            count++
        }

        bottomSheetDialog.findViewById<TextView>(R.id.lockScreen)?.setOnClickListener {
            val wallpaperManager =
                WallpaperManager.getInstance(this.requireContext().applicationContext)
            Glide.with(it).asBitmap().load(args.url).into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    wallpaperManager.setBitmap(resource, null, true, WallpaperManager.FLAG_LOCK)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
        }
        bottomSheetDialog.show()
    }
}
