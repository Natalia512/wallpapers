package com.example.artsukevich_p3.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.data.model.repository.RepositoryQueries
import com.example.domain.model.EntityQuery
import com.example.lesson22.ui.base.BaseViewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ViewModelQuery: BaseViewModel<EntityQuery>() {
    val repo: RepositoryQueries = RepositoryQueries()

    fun loadQueries(){
        val a=repo.getQueries()
        queries.postValue(a)
    }

    fun getByQuery(query:String?): EntityQuery?{
       return repo.getByQuery(query)
    }

    fun insertQuery(query: EntityQuery){
        repo.setQuery(query)
    }

    fun updateQuery(query: EntityQuery){
        repo.updateQuery(query)
    }

    fun deleteQuery(query: EntityQuery){
        repo.deleteQuery(query)
    }

    fun getQueryById(id:Long): EntityQuery? {
       return repo.getById(id)

    }
    fun createEntityQueryModel(query: String?,size:String): EntityQuery {
        val timeFormat: DateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val entityQuery = EntityQuery()
        if ( getByQuery(query)==null){
            entityQuery.query = query
            entityQuery.results = size.toString()
            entityQuery.date = timeFormat.format(Date())
           insertQuery(entityQuery)
        }else{
            updateQuery(entityQuery)
        }

        return  entityQuery
    }

}