package com.example.artsukevich_p3.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.data.NetworkService
import com.example.data.model.pojo.photo.SearchPhoto
import com.example.data.model.repository.RepositoryPhotos
import com.example.data.source.remote.ImagePageSource
import com.example.domain.model.EntityPhoto
import com.example.domain.model.NavigateModel
import com.example.lesson22.ui.base.BaseViewModel
import kotlinx.coroutines.flow.Flow

class ViewModelPhoto: BaseViewModel<EntityPhoto>() {
    val repo: RepositoryPhotos= RepositoryPhotos()

    fun loadPhotos(){
       val a=repo.getPhotos()
        queries.postValue(a)
    }

    fun insertPhotos(photo:NavigateModel){
        val ep= EntityPhoto()
        ep.navigateModel=photo
        repo.setPhotos(ep)
    }

    fun deletePhoto(photo: NavigateModel){
        val ep= EntityPhoto()
        ep.navigateModel=photo
        repo.deletePhoto(ep)
    }

    fun search(query:String?): Flow<PagingData<SearchPhoto>> {
        val passengers = Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { ImagePageSource(NetworkService.API, query) }
        ).flow.cachedIn(viewModelScope)


    return passengers}

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }
}