package com.example.artsukevich_p3.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.FavouriteScreenBinding
import com.example.artsukevich_p3.ui.adapter.AdapterViewPager
import com.example.artsukevich_p3.ui.adapter.FavouritesPhotoAdapter
import com.example.artsukevich_p3.ui.adapter.FavouritesQueriesAdapter
import com.example.artsukevich_p3.ui.viewmodels.ViewModelFavouriteQuery
import com.example.artsukevich_p3.ui.viewmodels.ViewModelPhoto
import com.example.artsukevich_p3.ui.viewmodels.ViewModelQuery
import com.example.data.mapper.mapToEntityPhotoList
import com.example.domain.model.EntityFavouriteQuery
import com.example.domain.model.NavigateModel
import com.google.android.material.tabs.TabLayoutMediator

class FavouritesScreenFragment : BaseFragment(R.layout.favourite_screen) {
    val viewModel = ViewModelPhoto()
    val viewModel2 = ViewModelFavouriteQuery()
    val viewVodel3 = ViewModelQuery()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FavouriteScreenBinding.bind(view)
        val viewPager = binding.viewPager

        val adapter = FavouritesPhotoAdapter(fun(i: NavigateModel) { clickImage(i) },
            fun(item: NavigateModel) { clearPhoto(item) })
        val adapter2 =
            FavouritesQueriesAdapter(fun(item: EntityFavouriteQuery) { clearQuery(item) })
        viewModel.loadPhotos()
        viewModel2.loadQueries()
        viewModel.getQueries().observe(viewLifecycleOwner, Observer {

            adapter.submitList(it.mapToEntityPhotoList())
        })
        viewModel2.getQueries().observe(viewLifecycleOwner, Observer {
            adapter2.submitList(it)
        })

        viewPager.adapter = AdapterViewPager(adapter, adapter2)
        viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        TabLayoutMediator(binding.navigat, viewPager) { tab, position ->
            when (position) {
                0 -> tab.icon = resources.getDrawable(R.drawable.ic_mage_menu)
                1 -> tab.icon = resources.getDrawable(R.drawable.ic_bookmark_menu)


            }
        }.attach()

    }


    fun clickImage(i: NavigateModel) {
        val action =
            FavouritesScreenFragmentDirections.actionFavouritesScreenFragmentToImageDetailsScreenFragment2(
                i
            )
        navigate(action)
    }

    fun clearPhoto(item: NavigateModel) {
        viewModel.deletePhoto(item)
        viewModel.loadPhotos()
    }

    fun clearQuery(item: EntityFavouriteQuery) {
        val itemQuery = viewVodel3.getQueryById(item.id)
        itemQuery?.fav = false
        viewVodel3.updateQuery(itemQuery!!)
        viewVodel3.loadQueries()
        viewModel2.deleteQuery(item)
        viewModel2.loadQueries()
        toast("QQQ")

    }
}