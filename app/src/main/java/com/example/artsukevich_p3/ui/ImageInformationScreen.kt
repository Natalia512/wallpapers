package com.example.artsukevich_p3.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment
import com.example.artsukevich_p3.databinding.ImageInformationScreenBinding
import com.example.domain.model.NavigateModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.DateFormat
import java.text.SimpleDateFormat


class ImageInformationScreen : BaseFragment(R.layout.image_information_screen) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ImageInformationScreenBinding.bind(view)
        val args: ImageInformationScreenArgs by navArgs()
        val navigateModel = args.navModel
        val bottomSheetDialog = BottomSheetDialog(this.requireContext())
        val a: ConstraintLayout = view.findViewById(R.id.info)
        val bb: Button = view.findViewById(R.id.portfolio)
        bottomSheetDialog.setContentView(R.layout.image_information_sheet)
        val icon = a.findViewById<ImageView>(R.id.imageUser)!!
        Glide.with(this.requireContext()).load(navigateModel.userImage).into(icon)
        Glide.with(this.requireContext()).load(navigateModel.url)
            .into(binding.imageInformationScreen)


        bb.setOnClickListener {
            if (navigateModel.portfolioUrl != null) {
                Log.i("URL", navigateModel.portfolioUrl.toString())
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(navigateModel.portfolioUrl!!))
                startActivity(intent)
            } else {
                toast("Portfolio not exist")
            }
        }
        val mBehavior = BottomSheetBehavior.from(a)
        if (mBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        } else {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }
        setBottomSheet(navigateModel, a)
    }

    fun setBottomSheet(navigateModel: NavigateModel, bottomSheetDialog: ConstraintLayout) {
        val timeFormat: DateFormat = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        var str = navigateModel.date
        bottomSheetDialog.findViewById<TextView>(R.id.name)?.text = navigateModel.userName
        bottomSheetDialog.findViewById<TextView>(R.id.email)?.text = navigateModel.userMail
        bottomSheetDialog.findViewById<TextView>(R.id.titleUser)?.text = navigateModel.description
        bottomSheetDialog.findViewById<TextView>(R.id.instagram)?.text = navigateModel.userInstagram
        bottomSheetDialog.findViewById<TextView>(R.id.twitter)?.text = navigateModel.userTwitter
        bottomSheetDialog.findViewById<TextView>(R.id.date)?.text = str.substring(0, 10)
        bottomSheetDialog.findViewById<TextView>(R.id.color)?.text = navigateModel.color
        bottomSheetDialog.findViewById<TextView>(R.id.width)?.text = navigateModel.width
        bottomSheetDialog.findViewById<TextView>(R.id.height)?.text = navigateModel.height
    }
}


