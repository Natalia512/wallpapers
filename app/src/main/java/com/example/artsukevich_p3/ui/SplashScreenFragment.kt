package com.example.artsukevich_p3.ui

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.common.base.BaseFragment

class SplashScreenFragment : BaseFragment(R.layout.splash_screen) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler().postDelayed({
            navigate(R.id.action_splashScreenFragment_to_searchScreenFragment)
        }, 1000)
    }
}
//https://habr.com/ru/post/352334/
//https://www.youtube.com/watch?v=JoFN10FDm8U
//https://www.youtube.com/watch?v=W0ag98EDhGc&list=PLQkwcJG4YTCSYJ13G4kVIJ10X5zisB2Lq&index=3
//https://jtmuller5-98869.medium.com/fragment-transitions-with-shared-elements-using-android-navigation-7dcfe01aacd
//https://medium.com/@serbelga/shared-elements-in-android-navigation-architecture-component-bc5e7922ecdf
//https://www.youtube.com/watch?v=Y19tdGRy2LQ
//https://blog.sebastiano.dev/playing-with-elevation-in-android-part-2/
//https://jtmuller5-98869.medium.com/fragment-transitions-with-shared-elements-using-android-navigation-7dcfe01aacd
//http://www.ohandroid.com/layout_collapseparallaxmultiplier-collapsingtool.html
//https://medium.com/@serbelga/shared-elements-in-android-navigation-architecture-component-bc5e7922ecdf
//https://xakep.ru/2016/03/01/material-design-2/
//https://www.youtube.com/watch?v=9zVmYVDTcEM
//https://www.youtube.com/watch?v=2WaKMd1IAvo
//https://www.youtube.com/watch?v=y2M8gLBUeW4&list=RDCMUC_Fh8kvtkVPkeihBs42jGcA&start_radio=1&rv=y2M8gLBUeW4&t=587
//https://www.youtube.com/watch?v=7GuZMutlJmE
//https://www.youtube.com/watch?v=mZats9mHIYE
//https://developer.android.com/codelabs/android-paging#3