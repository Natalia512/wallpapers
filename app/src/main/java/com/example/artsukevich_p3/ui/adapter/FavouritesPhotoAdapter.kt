package com.example.artsukevich_p3.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.artsukevich_p3.R
import com.example.artsukevich_p3.databinding.FavouriteItemPhotoBinding
import com.example.domain.model.NavigateModel

class FavouritesPhotoAdapter(private val clickImage:(i: NavigateModel)->Unit,private val deletePhoto:(item: NavigateModel)->Unit) :
    ListAdapter<NavigateModel, FavouritesPhotoAdapter.PhotoViewHolder>(PhotoDiffUtil()) {

    open inner class PhotoViewHolder(private val binding: FavouriteItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val image=binding.image

        fun bind(item: NavigateModel) = with(itemView) {
            val animationController = AnimationUtils.loadAnimation(context, R.anim.fall_down)
            itemView.animation=animationController
            Log.i("Q3", item.toString())
            Glide.with(context)
                .load(item.url)
                .into(image)
            image.setOnClickListener {
                clickImage(item)
            }
            binding.fab.setOnClickListener {
                deletePhoto(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
        val binding = FavouriteItemPhotoBinding.inflate(view, parent, false)
        return PhotoViewHolder(binding)
    }



    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
        Log.i("Q3","item.toString()")
    }

    class PhotoDiffUtil() : DiffUtil.ItemCallback<NavigateModel>() {

        override fun areItemsTheSame(oldItem: NavigateModel, newItem:NavigateModel): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: NavigateModel, newItem: NavigateModel): Boolean {
            return oldItem.date == newItem.date
        }
    }


}