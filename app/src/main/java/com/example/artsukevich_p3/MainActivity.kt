package com.example.artsukevich_p3

import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.example.artsukevich_p3.databinding.ActivityMainBinding
import com.example.artsukevich_p3.util.view.Notifications

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Artsukevich_P3)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        Notifications().createNotificationChanel(this)
        val bottomNavigation = binding.navigation!!
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_) as NavHostFragment
        val navController = navHostFragment.navController
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                100
            )
        }
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            100
        )

        bottomNavigation?.setupWithNavController(navController)
        NavigationUI.setupWithNavController(bottomNavigation, navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.imageDetailsScreenFragment || destination.id == R.id.splashScreenFragment ||
                destination.id == R.id.imageFullscreenFragment || destination.id == R.id.imageInformationScreen
            ) {
                bottomNavigation.visibility = View.GONE
            } else {
                bottomNavigation.visibility = View.VISIBLE
            }
            if (destination.id == R.id.searchScreenFragment) {
                window.statusBarColor = Color.WHITE
            }

        }
    }
}
