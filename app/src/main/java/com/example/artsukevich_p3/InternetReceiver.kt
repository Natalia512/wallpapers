package com.example.artsukevich_p3

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.app.NotificationManagerCompat
import com.example.artsukevich_p3.util.view.Notifications

class InternetReceiver(val connect: CardView) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!
        val mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)!!
        if (wifi.isConnected || mobile.isConnected) {
            connect.visibility = View.INVISIBLE
        } else {
            connect.visibility = View.VISIBLE
            with(NotificationManagerCompat.from(context)) {
                notify(
                    1001,
                    Notifications().buildNotification(
                        context,
                        "Internet connection",
                        "No internet connection "
                    ).build()
                )
            }
        }
    }
}