package com.example.domain.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "queries")
class EntityQuery {
    @PrimaryKey(autoGenerate = true)
    var id:Long=0
    var query:String?=""
    var results:String=""
    var date:String=""
    var fav:Boolean=false
}