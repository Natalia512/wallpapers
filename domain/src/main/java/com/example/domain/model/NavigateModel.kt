package com.example.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NavigateModel(var id:String,var url:String,
                         var date:String,
                         var color:String,
                         var width:String,
                         var height:String,
                         var userName:String?,
                         var userMail:String?,
                         var userInstagram:String?,
                         var userTwitter:String?,var userImage:String="", var description:String?,var portfolioUrl:String?):Parcelable
