package com.example.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favQueries")
class EntityFavouriteQuery {
    @PrimaryKey()
    var id:Long=0
    var query:String?=""
    var results:String=""
    var date:String=""
}