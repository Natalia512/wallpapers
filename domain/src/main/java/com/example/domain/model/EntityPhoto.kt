package com.example.domain.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(tableName = "photos")
class EntityPhoto {
    @PrimaryKey()
    var id:Long=0
    @Embedded(prefix = "prefix_")
    lateinit var navigateModel: NavigateModel
}
