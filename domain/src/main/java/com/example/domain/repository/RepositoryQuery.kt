package com.example.domain.repository

import com.example.domain.model.EntityQuery

interface RepositoryQuery {
    fun getQueries():List<EntityQuery>
    fun setQuery(query: EntityQuery)
    fun updateQuery(query:EntityQuery)
    fun deleteQuery(query:EntityQuery)
    fun getById(id:Long):EntityQuery?
    fun getByQuery(query:String?):EntityQuery?
}