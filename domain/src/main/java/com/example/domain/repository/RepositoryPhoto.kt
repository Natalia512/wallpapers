package com.example.domain.repository

import com.example.domain.model.EntityPhoto

interface RepositoryPhoto {
    fun getPhotos():List<EntityPhoto>
    fun setPhotos(photo:EntityPhoto)
    fun deletePhoto(photo: EntityPhoto)
}