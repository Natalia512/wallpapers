package com.example.domain.repository

import com.example.domain.model.EntityFavouriteQuery

interface RepositoryFavouriteQuery {
    fun getQueries():List<EntityFavouriteQuery>
    fun setQuery(query:EntityFavouriteQuery)
    fun deleteQuery(query:EntityFavouriteQuery)
}