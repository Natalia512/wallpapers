package com.example.data

import android.app.Application
import androidx.room.Room
import com.example.data.source.local.Database


class App:Application() {
    var database: Database?=null
        private set

    override fun onCreate() {
        super.onCreate()
        instance =this
        database=Room.databaseBuilder(this, Database::class.java,"database").build()
    }

    companion object {
        var instance: App?=null
    }
}
