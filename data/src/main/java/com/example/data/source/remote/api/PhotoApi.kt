package com.example.data.source.remote.api

import com.example.data.model.pojo.photo.SearchPhoto
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface PhotoApi {
    @GET("/search/photos")
  suspend fun searchPhoto( @Query("query") query: String?,@Query("page") page: Int,
                     @Query("per_page") pageLimit: Int): SearchPhoto
}