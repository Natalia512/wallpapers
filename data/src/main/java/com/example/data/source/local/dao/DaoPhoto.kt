package com.example.data.model.db

import androidx.room.*
import com.example.domain.model.EntityPhoto

@Dao
interface DaoPhoto {
    @Query("Select * from photos")
    suspend fun getAll():List<EntityPhoto>

    @Update
    fun update(photo:EntityPhoto)

    @Insert
    suspend fun insert(photo: EntityPhoto)

    @Delete
    suspend fun delete(photo: EntityPhoto)

//    @Transaction
//    @Query("SELECT * FROM users")
//    fun getUsersWithPlaylists(): List<UserWhithPhoto>

}