package com.example.data.model.db

import androidx.room.*
import com.example.domain.model.EntityPhoto
import com.example.domain.model.EntityQuery
@Dao
interface DaoQuery {
    @Query("Select * from queries")
    suspend fun getAll():List<EntityQuery>

    @Query("Select * from queries Where id =:id")
   suspend fun getById(id:Long): EntityQuery

    @Update
   suspend fun update(query: EntityQuery)

    @Insert
    suspend fun insert(query: EntityQuery)

    @Delete
    suspend fun delete(query: EntityQuery)

    @Query("Select * from queries Where `query` =:query")
    suspend fun getByQuery(query: String?):EntityQuery
}