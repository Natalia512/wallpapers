package com.example.data.source.remote

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.data.model.pojo.photo.Photos
import com.example.data.model.pojo.photo.SearchPhoto
import com.example.data.source.remote.api.PhotoApi
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

private const val GITHUB_STARTING_PAGE_INDEX = 1
class ImagePageSource(val service: PhotoApi, val query: String?): PagingSource<Int, SearchPhoto>(){
   var  b:ArrayList<Photos> = arrayListOf()
val SAVED_TEXT="saved size"
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SearchPhoto> {

        val position = params.key ?: GITHUB_STARTING_PAGE_INDEX
        val apiQuery = query
        var res : List<SearchPhoto> = emptyList()

       try {
         //  b = service.searchPhoto(apiQuery, position, params.loadSize).result

           res = listOf(service.searchPhoto(apiQuery, position, params.loadSize))

       }catch (e: UnknownHostException){
           Log.i("tt","tt")
       }
        return   try {

            val nextKey = if (b.isEmpty()) {
                Log.i("Q", "0")
                null
            } else {
                Log.i("Q", "10")
                position + (params.loadSize / 50)
            }

            LoadResult.Page(
                data = res,
                prevKey = if (position == GITHUB_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, SearchPhoto>): Int? {

        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}