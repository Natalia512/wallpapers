package com.example.data.model.db

import androidx.room.*
import com.example.domain.model.EntityFavouriteQuery
import com.example.domain.model.EntityQuery
@Dao
interface DaoFavouriteQuery {
    @Query("Select * from favQueries")
    suspend fun getAll():List<EntityFavouriteQuery>

    @Query("Select * from favQueries Where id =:id")
    fun getById(id:Long): EntityFavouriteQuery

    @Update
    suspend fun update(query: EntityFavouriteQuery)

    @Insert
    suspend fun insert(query: EntityFavouriteQuery)

    @Delete
    suspend fun delete(query: EntityFavouriteQuery)
}