package com.example.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.model.db.DaoFavouriteQuery
import com.example.data.model.db.DaoPhoto
import com.example.data.model.db.DaoQuery
import com.example.domain.model.EntityFavouriteQuery
import com.example.domain.model.EntityPhoto
import com.example.domain.model.EntityQuery

@Database(entities = arrayOf(EntityPhoto::class,EntityQuery::class,EntityFavouriteQuery::class),version = 1)
abstract class Database :RoomDatabase(){
    abstract fun daoPhoto(): DaoPhoto
    abstract fun daoQuery(): DaoQuery
    abstract fun daoFavouriteQuery(): DaoFavouriteQuery
}