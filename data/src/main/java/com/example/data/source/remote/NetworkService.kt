package com.example.data

import com.example.data.source.remote.api.PhotoApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object NetworkService{
        private var mInstance: NetworkService? = null
        private val BASE_URL = "https://api.unsplash.com/"
        private var mRetrofit: Retrofit? = null
        private val retrofit by lazy {
            val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("Authorization","Client-ID " + "eUkfbwI9ZzhUUS9IM_qKPXgIJ_YNrqiHzaqPTwR88fo")
                        .build()
                    chain.proceed(request)
                })
                .build()
            Retrofit.Builder()
                .baseUrl(BASE_URL)
              //  .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        }
        val API : PhotoApi by lazy {
            retrofit.create(PhotoApi::class.java)
        }
    }