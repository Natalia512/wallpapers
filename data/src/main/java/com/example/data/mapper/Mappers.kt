package com.example.data.mapper

import com.example.data.model.pojo.photo.Photos
import com.example.domain.model.EntityPhoto
import com.example.domain.model.NavigateModel

fun Photos.mapToPhoto()=NavigateModel(id = id,url=urls.regular,date=create_at,color=color,width=width,height=height,userName = user.name,userMail = user.username,
userInstagram =user.instagram,userTwitter =user.twitter,userImage = user.profileImage.small,description=description,portfolioUrl = user.portfolioUrl)

fun List<Photos>.mapToPhotosList():List<NavigateModel>{
    return this.map{
        it.mapToPhoto()
    }}

    fun EntityPhoto.mapEntityPhoto()=NavigateModel(url=navigateModel.url,date=navigateModel.date,color=navigateModel.color,width=navigateModel.width,
        height=navigateModel.height,userName = navigateModel.userName,userMail = navigateModel.userMail,
        userInstagram =navigateModel.userInstagram,userTwitter =navigateModel.userTwitter,userImage =navigateModel.userImage,
        description=navigateModel.description,portfolioUrl = navigateModel.portfolioUrl,id = navigateModel.id)

    fun List<EntityPhoto>.mapToEntityPhotoList():List<NavigateModel>{
        return this.map{
            it.mapEntityPhoto()
        }
}
