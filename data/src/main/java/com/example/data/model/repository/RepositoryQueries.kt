package com.example.data.model.repository

import com.example.data.App
import com.example.data.source.local.Database
import com.example.domain.model.EntityQuery
import com.example.domain.repository.RepositoryQuery
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class RepositoryQueries:RepositoryQuery {
    private val db: Database
    init {
        db = App.instance?.database!!
    }
    override fun getQueries(): List<EntityQuery> {
        lateinit var list:List<EntityQuery>
        runBlocking {
            val job=launch {
                list= db.daoQuery().getAll()
            }
            job.join()
        }

        return list
    }



    override fun setQuery(query: EntityQuery) {
        runBlocking {
            val job= launch {
                db.daoQuery().insert(query)
            }
            job.join()
        }

    }

    override fun updateQuery(query: EntityQuery) {
        runBlocking {
            val job= launch {
                db.daoQuery().update(query)
            }
            job.join()
        }
    }

    override fun deleteQuery(query: EntityQuery) {
        runBlocking {
            val job= launch {
                db.daoQuery().delete(query)
            }
            job.join()
        }
    }

    override fun getById(id: Long): EntityQuery? {
        var query: EntityQuery?=null
        runBlocking {
            val job= launch {
              query=  db.daoQuery().getById(id)
            }
            job.join()
        }
        return query
    }

    override fun getByQuery(query: String?): EntityQuery? {
        var entityQuery: EntityQuery?=null
        runBlocking {
            val job= launch {
                entityQuery=  db.daoQuery().getByQuery(query)
            }
            job.join()
        }
        return entityQuery
    }
}