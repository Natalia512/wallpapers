package com.example.data.model.pojo.user

import com.google.gson.annotations.SerializedName


data class User(@SerializedName("id")  val id: String,
                @SerializedName("username")  val username: String,
                @SerializedName("name") val name : String,
                @SerializedName("profile_image")  val profileImage : ProfileImage,
                @SerializedName("total_photos")  val TotalPhotos: String,
                @SerializedName("total_collections")  val collection: String,
                @SerializedName("instagram_username") val instagram:String, @SerializedName("twitter_username") val twitter:String,
                @SerializedName("description") val description:String,
                @SerializedName("portfolio_url") val portfolioUrl:String
)
