package com.example.data.model.pojo.photo

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class PhotoUrl(@SerializedName("raw") @Expose val raw: String,
                    @SerializedName("full") @Expose val full: String,
                    @SerializedName("regular") @Expose val regular: String,
                    @SerializedName("small") @Expose val small: String)