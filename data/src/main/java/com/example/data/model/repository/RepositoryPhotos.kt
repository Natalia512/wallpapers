package com.example.data.model.repository

import com.example.data.App
import com.example.data.source.local.Database
import com.example.domain.model.EntityPhoto

import com.example.domain.repository.RepositoryPhoto
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class RepositoryPhotos():RepositoryPhoto{

    private val db: Database

    init {
       db = App.instance?.database!!
    }


    override fun getPhotos(): List<EntityPhoto> {
       lateinit var list:List<EntityPhoto>
      runBlocking {
         val job=launch {
             list= db.daoPhoto().getAll()
            }
          job.join()
        }

        return list
    }

    override fun setPhotos(photo: EntityPhoto) {
        runBlocking {
            val job= launch {
                db.daoPhoto().insert(photo)
            }
            job.join()
        }

    }

    override fun deletePhoto(photo: EntityPhoto) {
        runBlocking {
            val job= launch {
                db.daoPhoto().delete(photo)
            }
            job.join()
        }
    }

//    override suspend fun responseFun(response: Call<SearchPhoto>): ArrayList<Photos> {
//        var result:ArrayList<Photos> = arrayListOf()
////        runBlocking{
////     getNumbers(response).collect { user-> result= user
////         Log.i("TAGG",user.size.toString()) }
////        }
//        result = response.execute().body()?.result!!
//        return result
//    }

//    fun getNumbers(response: Call<SearchPhoto>): Flow<ArrayList<Photos>> = flow{
//
//            emit(response.execute().body()?.result!!)
//    }


}