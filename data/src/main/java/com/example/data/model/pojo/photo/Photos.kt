package com.example.data.model.pojo.photo

import com.example.data.model.pojo.user.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Photos(@SerializedName("id") @Expose val id: String, @SerializedName("width") @Expose val width: String,
                  @SerializedName("height") @Expose val height: String,
                  @SerializedName("color") @Expose val color: String, @SerializedName("description") @Expose val description: String,
                  @SerializedName("user") @Expose val user: User, @SerializedName("urls") @Expose val urls: PhotoUrl,
                  @SerializedName("likes") @Expose val likes:String, @SerializedName("blur_hash") @Expose val blur_hash:String,
                  @SerializedName("created_at") @Expose val create_at: String)


