package com.example.data.model.pojo.photo

import com.example.data.model.pojo.photo.Photos
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchPhoto(
    @SerializedName("results")@Expose val result: ArrayList<Photos>,
    @SerializedName("total")@Expose val total:Int
)
