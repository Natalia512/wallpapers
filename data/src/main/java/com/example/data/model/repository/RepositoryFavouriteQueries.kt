package com.example.data.model.repository

import com.example.data.App
import com.example.data.source.local.Database

import com.example.domain.model.EntityFavouriteQuery
import com.example.domain.repository.RepositoryFavouriteQuery
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class RepositoryFavouriteQueries:RepositoryFavouriteQuery {
    private val db: Database
    init {
        db = App.instance?.database!!
    }
    override fun getQueries(): List<EntityFavouriteQuery> {
        lateinit var list:List<EntityFavouriteQuery>
        runBlocking {
            val job=launch {
                list= db.daoFavouriteQuery().getAll()
            }
            job.join()
        }

        return list
    }

    override fun setQuery(query: EntityFavouriteQuery) {
        runBlocking {
            val job= launch {
                db.daoFavouriteQuery().insert(query)
            }
            job.join()
        }

    }

    override fun deleteQuery(query: EntityFavouriteQuery) {
        runBlocking {
            val job= launch {
                db.daoFavouriteQuery().delete(query)
            }
            job.join()
        }
    }
}